<link href="<?= base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="<?= base_url(); ?>/assets/css/style.css" rel="stylesheet" media="screen">


<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="brand" href="#">Hotel Info</a>
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active"><a href="#">Hotel #103</a></li>
                    <li><a href="#about">Hotel list</a></li>
                    <li><a href="#contact">Contact hotel reception</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Actions <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Book this hotel</a></li>
                            <li><a href="#">Leave a review</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Report abuse</a></li>
                        </ul>
                    </li>
                </ul>
                <form class="navbar-form pull-right">
                    <input class="span2" type="text" placeholder="Email">
                    <input class="span2" type="password" placeholder="Password">
                    <button type="submit" class="btn">Sign in</button>
                </form>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span1">
            &nbsp;
        </div>
        <div class="span11">
            <table class="table table-striped">
                <caption>Hotel Info on <strong>Hotel #103</strong></caption>
                <thead>
                    <tr>
                        <th>Hotel title</th>
                        <th>Address</th>
                        <th>Contact e-mail</th>
                        <th>URL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $hotelInfo->name ?></td>
                        <td><?= $hotelInfo->address->country ?>, <?= $hotelInfo->address->city ?>, <?= $hotelInfo->address->street ?> <?= $hotelInfo->address->zip ?></td>
                        <td><?= $hotelInfo->address->mail->address ?></td>
                        <td><a href='<?= $hotelInfo->url ?>'><?= $hotelInfo->url ?></a></td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-striped">
                <caption>Stay Packages information on <strong>Hotel #103</strong></caption>
                <thead>
                    <tr>
                        <th class='t_number'>St. Pkg. ID</th>
                        <th class='t_name'>Name</th>
                        <th class='t_description'>Description</th>
                        <th class='t_number'># of nights</th>
                        <th class='t_note'>Note</th>
                        <th class='t_number'>Gallery ID</th>
                        <th class='t_includes'>Includes</th>
                        <th class='t_number'>Unit ID</th>
                        <th class='t_order'>Make order</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($hotelStayPackages->stayPackage as $stay_package): ?>
                        <tr>
                            <td><span class='t_bold'><?= $stay_package->styId ?></span></td>
                            <td><?= $stay_package->name ?></td>
                            <td><?= $stay_package->description ?></td>
                            <td><?= $stay_package->nights ?></td>
                            <td><?= $stay_package->note ?></td>
                            <td><?= $stay_package->galId ?></td>
                            <td><?= $stay_package->includes ?></td>
                            <td><?= $stay_package->unitId ?></td>
                            <td><button class='rounded_button' alt='Order Now!'>Order Now!</button></td>
                        </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<footer>
    <p class="pull-right"><a href="#">Back to top</a></p>
    <p>&copy; 2013 Hotel Info, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
</footer>


<script src="http://code.jquery.com/jquery.js"></script>
<script src="<?= base_url(); ?>/assets/js/bootstrap.min.js"></script>